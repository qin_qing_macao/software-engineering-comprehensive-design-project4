CREATE DATABASE HouseRent
GO

USE HouseRent
GO

--1.注册用户表 Users ---------------------------------------------------------------------

CREATE TABLE WebUsers(
	wId int identity(1,1) primary key,
	wName varchar(20)  NOT NULL,    
	wPhone nvarchar(20) NOT NULL,               /* 用户手机 */
	wEmail nvarchar(20) NOT NULL,               /* 用户电子邮件地址 */
	wBirthday nvarchar(10),                     /* 用户出生日期 */
	wSex char(2),                               /* 用户性别 */
	wPwd nvarchar(20) NOT NULL,                 /* 用户密码 */
	wImage char(20),                            /* 用户头像 */
)
GO
INSERT INTO WebUsers
VALUES
('张亮','18953249771','1535076341@qq.com','1998-12-21','女','1232@a','1.jpg'),
('陈明','18953249772','1535076342@qq.com','1998-12-22','男','1231@b','2.jpg'),
('陈红','18953249773','1535076343@qq.com','1998-12-23','女','1231@c','3.jpg'),
('王丽','17856242642','1785624264@qq.com','1999-02-12','女','1231@d','4.jpg')
GO
SELECT * FROM WebUsers
GO
DROP TABLE WebUsers

GO
--2.注册中介用户表 IntermediaryUsers ---------------------------------------------------------------------

CREATE TABLE IntermediaryUsers(
    uId int identity(1,1) primary key,
	uName varchar(20) NOT NULL,     
	uPhone nvarchar(20) NOT NULL,               /* 中介手机 */
	uEmail nvarchar(20) NOT NULL,               /* 中介电子邮件地址 */
	uBirthday nvarchar(10),                     /* 中介出生日期 */
	uSex char(2),                               /* 中介性别 */
	uPwd nvarchar(20) NOT NULL,                 /* 中介密码 */
	uImage char(20),                            /* 中介头像 */
	keyword nvarchar(50)                        /*检索关键字*/        
)
GO
insert into IntermediaryUsers values
('小张','18953249771','1535076341@qq.com','1998-12-21','女','123@a','1.jpg','小张1535076341@qq.com18953249771'),
('小明','18953249772','1535076342@qq.com','1998-12-22','男','123@b','2.jpg','小明1535076342@qq.com18953249772'),
('小红','18953249773','1535076343@qq.com','1998-12-23','女','123@c','3.jpg','小红1535076343@qq.com18953249773')
select*from IntermediaryUsers
drop table IntermediaryUsers

--3.管理员表 Managers ---------------------------------------------------------------------

CREATE TABLE Managers(
	MID int primary key identity(1,1) NOT NULL,  /* 管理员编号 主键 自动增长 */
	MName nvarchar(20) NOT NULL,                 /* 管理员昵称 */
	MPwd nvarchar(20) NOT NULL                   /* 管理员密码 */
)
GO

--4.房源信息 HouseInfo ---------------------------------------------------------------------

create table HouseImfo(
	hid nvarchar(20) primary key,
	uId int references IntermediaryUsers(uId),
	price float,          --价格
	location nvarchar(50),--地区
	houseType nvarchar(20),--户型
	area float,           --平方
	address nvarchar(50),--详细地址
	keyword nvarchar(50),--关键字
	hpic varchar(20)
)
select * from HouseImfo
drop table HouseImfo
insert into HouseImfo  values('H0001',1,1230,'市北区','二室',345,'市北区威海路201号','小张市北区威海路二室','001.jpg')
insert into HouseImfo  values('H0002',1,2000,'市南区','三室',420,'市南区香港路103号','小张市南区香港路三室','002.jpg')
insert into HouseImfo  values('H0003',3,1800,'崂山区','二室',180,'崂山区崂山路20号','小红崂山区崂山路二室','002.jpg')
insert into HouseImfo  values('H0004',3,1900,'李沧区','一室',120,'李沧区延吉路80号','小红李沧区延吉路一室','002.jpg')
insert into HouseImfo  values('H0005',2,1500,'城阳区','二室',200,'城阳区城阳路70号','小明城阳区城阳路二室','002.jpg')

--5.用户房屋表 UserHouse ---------------------------------------------------------------------

CREATE TABLE UserHouse(
	UName varchar(20) foreign key references  Users(UName) NOT NULL,     /* 用户昵称 外键 */
	HDescription varchar(50),                    /* 房屋描述 */
    HLocation nvarchar(50) NOT NULL,             /* 房屋位置 */
	HTraffic varchar(20),                        /* 交通状况 */
	HConfig varchar(20),                         /* 设施配置 */
	HPay nvarchar(10),                           /* 房租信息 */
	HType char(20) NOT NULL,                     /* 房屋类型 */
	Hpic char(20) NOT NULL                       /* 房屋图片 */    
)
GO
select * from UserHouse
GO