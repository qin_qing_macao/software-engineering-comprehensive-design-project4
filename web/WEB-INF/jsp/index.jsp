<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>home</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <base href="${pageContext.request.contextPath}/">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">		
        <link rel="stylesheet" href="css/indexstyle.css">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
    </head>
    <body>
                <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-6">
                        <div class="phone-mail-area">
                            <ul>
                                <li>
                                    <a href="#"><i class="fa fa-phone"></i>+123 456 789 000</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-envelope-o"></i>admin@QDUNiit.com</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-6">
                        <div class="login-bookmark-area">
                            <div class="register-login">
                                <a href="#"><i class="fa fa-user"></i>登录</a>
                                <a href="#"><i class="fa fa-sign-in"></i>注册</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <header class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- Logo Start -->
                    <div class="logo-wrap">
                        <a href="#"><img src="image/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="main-menu">
                        <nav>
                            <ul id="nav">
                                <li class="active"><a href="index">Home</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">房屋租赁网版本 2</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">房产</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">loft公寓</a></li>
                                        <li><a href="#">双人公寓</a></li>
                                        <li><a href="#">单人公寓</a></li>
                                    </ul>
                                </li>
                                <li><a href="manager_index">管理员</a>
                                    <ul class="sub-menu">
                                        <li><a href="#">管理员登录</a></li>
                                        <li><a href="#">注：管理员注册需我们联系！</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">页面</a>
                                    <div class="mega-menu">
                                        <span>
                                            <a href="#">官方：</a>
                                            <a href="#">联系我们</a>
                                            <a href="#">团队 </a>
                                        </span>
                                        <span>
                                            <a href="#">公寓类型：</a>
                                            <a href="#">loft公寓 </a>
                                            <a href="#">双人公寓 </a>
                                            <a href="#">单人公寓 </a>
                                        </span>
                                        <span>
                                            <a href="#">常见问题：</a>
                                            <a href="#">官方Blog</a>
                                            <a href="#">新浪微博</a>
                                            <a href="#">微信公众号</a>
                                        </span>
                                    </div>
                                </li>
                                <li><a href="#">FAQs</a></li>
                                <li><a href="#">联系我们</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
  
    <section class="slider-section ">
        <div class="item">
            <div class="item">
                <img src="image/slider/1.jpg" alt="">
                <div class="slide-content carousel-caption hidden-xs">
                    <div class="slide-content-top">
                        <h1>轻松之家</h1>
                        <h2>青岛-市南区五四广场-东海西路12号</h2>
                        <p><br>这是一间德式老别墅，在德式别墅基础上改造的怀旧风格loft。与海军博物馆一墙之隔，步行一分钟到达海边，拍照网红街大学路位于周边，可供游玩。<br></p>
                    </div>
                    <div class="slide-property-detail">
                        <ul>
                            <li>&nbsp;4间卧室</li>
                            <li>&nbsp;独立浴室</li>
                            <li>&nbsp;345㎡ </li>
                            <li>&nbsp;¥30,000,000</li>
                            <li class="slider-btn">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#">&nbsp;&nbsp;&nbsp;查看详情&nbsp;&nbsp;&nbsp;</a>
                            </li> 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="image/slider/2.jpg" alt="">
            <div class="slide-content carousel-caption hidden-xs">
                <div class="slide-content-top">
                    <h1>享受型公寓</h1>
                    <h2>青岛-市南区-海阳路10号/东海西路2号</h2>
                    <p>美墅假期公寓  地处东海西路2号  毗邻八大关旅游风景区、五四广场、奥帆中心、第二第三海水浴场，以及市政府、青岛CBD、台东商业区等行政商业中心。独栋海景别墅公寓，欧式的乡村风格，环境优雅
                    ，设施精美。入住于此，您可隔窗临海。午后市静享庭院小憩，夜晚漫步沙滩看潮。</p>
                </div>
                <div class="slide-property-detail">
                    <ul>
                        <li>&nbsp;4间卧室</li>
                        <li>&nbsp;独立浴室</li>
                        <li>&nbsp;256㎡</li>
                        <li>&nbsp;¥20,000,000</li>
                        <li class="slider-btn">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#">&nbsp;&nbsp;&nbsp;查看详情&nbsp;&nbsp;&nbsp;</a>
                        </li> 
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="property-query-area">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="query-title">
                        <h2>搜索你想要的</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <form action="#">
                    <div class="col-md-3 col-sm-6">
                        <div class="single-query">
                            <label for="keyword-input">关键字</label>
                            <input type="text" id="keyword-input" placeholder="请输入关键词">
                            <label>房型</label>
                            <select name="Any">
                                <option value="any" selected>Any</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="single-query">
                            <label>房产类型</label>
                            <select>
                                <option value="any" selected>Any</option>
                                <option value="home">Home</option>
                                <option value="resort">Resort</option>
                                <option value="land">Land</option>
                                <option value="Restrurent">Restrurent</option>
                            </select>
                            <label>卫浴类型</label>
                            <select>
                                <option value="any" selected>Any</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="single-query">
                            <label>坐落位置</label>
                            <select>
                                <option value="any" selected>Any</option>
                                <option value="new york">new york</option>
                                <option value="London">London</option>
                                <option value="kosovo">kosovo</option>
                                <option value="Los Angeles">Los Angeles</option>
                            </select>
                            <label>最低价位</label>
                            <select>
                                <option value="any">any</option>
                                <option value="$200">$200</option>
                                <option value="$2000">$2000</option>
                                <option value="$20000">$20000</option>
                                <option value="$200000">$200000</option>
                                <option value="$2000000">$2000000</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="single-query">
                            <label>占地平方</label>
                            <select>
                                <option value="any" selected>Any</option>
                                <option value="1000 sf">1000 sf</option>
                                <option value="2000 sf">2000 sf</option>
                                <option value="3000 sf">3000 sf</option>
                                <option value="4000 sf">4000 sf</option>
                                <option value="5000 sf">5000 sf</option>
                            </select>
                            <label>最高价格</label>
                            <select>
                                <option value="any" selected>Any</option>
                                <option value="$3000">$3000</option>
                                <option value="$4000">$4000</option>
                                <option value="$5000">$5000</option>
                                <option value="$6000">$6000</option>
                                <option value="$7000">$7000</option>
                            </select>
                        </div>
                        <div class="query-submit-button pull-right">
                            <form action="">
                                <input type="submit" value="&nbsp;搜索&nbsp;">
                            </form>
                        </div>  
                    </div>
                </form>
            </div>
        </div>
    </section>

    <section class="welcome-area area-pading fix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-heading-style-one text-center">
                        <h2>新房，新的生活</h2>
                        <p> 租女友过年，不如先租房存钱! <br>租有所需，有求必应;足不出户，租天下屋.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="single-promotion text-center wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                        <div class="promo-icon">
                            <i class="fa fa-home"></i>
                        </div>
                        <div class="promo-content">
                            <h3><a href="#">稳定的房源</a></h3>
                            <p>这里有最稳定的房源，这里有最舒适的服务，这里还有最顾客至上的态度！</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="single-promotion text-center wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".5s">
                        <div class="promo-icon">
                            <i class="fa fa-street-view"></i>
                        </div>
                        <div class="promo-content">
                            <h3><a href="#">观尽天下房</a> </h3>
                            <p>足出不户，上网搜中舒心房。做个有品味的人，首先你要先找个有气质的房子！</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="single-promotion text-center wow fadeInUp" data-wow-duration="1.5s" data-wow-delay=".7s">
                        <div class="promo-icon">
                            <i class="fa fa-user"></i>
                        </div>
                        <div class="promo-content">
                            <h3><a href="#">加入我们！</a> </h3>
                            <p>想成为房屋代理？想拿到一手房源？或者是想了解我们的日常工作生活？别犹豫啦，那就请加入我们吧！</p>
                        </div>
                    </div>
                </div> 
            </div>
        </div> 
    </section>

    <section class="properties-area area-pading fix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="properties-title">
                        <h2>特色房产</h2>
                        <a href="#" class="view-more">&nbsp;查看全部&nbsp;</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="single-featured-properties">
                        <div class="properties-image">
                            <a href="#"><img src="image/properties/1.jpg" alt=""></a>
                        </div>
                        <div class="sale-tag">
                            <p>FOR SALE</p>
                        </div>
                        <div class="properties-include">
                            <ul>
                                <li><i class="fa fa-home"></i> 30,000 Acres</li>
                                <li><i class="fa fa-bed"></i> 4 Bedrooms</li>
                            </ul>
                        </div>
                        <div class="properties-content">
                            <h3><a href="#">South Mervin Boulevard</a></h3>
                            <p><i class="fa fa-map-marker"></i> Merrick Way, Miami, USA</p>
                            <p class="detail-text">Lorem ipsum dolor sit amet,consectetuer adipiing elit. </p>
                            <div class="price-detail">
                                <p class="price-range pull-left">$ 1,23,00.000</p>
                                <a href="#" class="price-detail pull-right">Details <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div> 
                
                <div class="col-md-4 col-sm-6">
                    <div class="single-featured-properties">
                        <div class="properties-image">
                            <a href="#"><img src="image/properties/2.jpg" alt=""></a>
                        </div>
                        <div class="sale-tag">
                            <p>&nbsp;&nbsp;待售&nbsp;&nbsp;</p>
                        </div>
                        <div class="properties-include">
                            <ul>
                                <li><i class="fa fa-home"></i> 365 ㎡</li>
                                <li><i class="fa fa-bed"></i>三室两厅</li>
                                <li><i class="fa fa-tty"></i> 独立卫浴</li>
                            </ul>
                        </div>
                        <div class="properties-content">
                            <h3><a href="#">东海西路12号</a></h3>
                            <p><i class="fa fa-map-marker"></i> 沿海, 市南区, 青岛</p>
                            <p class="detail-text">毗邻五四广场、奥帆中心、第二第三海水浴场，以及市政府、台东商业区等行政商业中心。 </p>
                            <div class="price-detail">
                                <p class="price-range pull-left">¥ 3,000,000</p>
                                <a href="#" class="price-detail pull-right">Details <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-6">
                    <div class="single-featured-properties">
                        <div class="properties-image">
                            <a href="#"><img src="image/properties/3.jpg" alt=""></a>
                        </div>
                        <div class="sale-tag">
                            <p>&nbsp;&nbsp;待售&nbsp;&nbsp;</p>
                        </div>
                        <div class="properties-include">
                            <ul>
                                <li><i class="fa fa-home"></i> 221 ㎡</li>
                                <li><i class="fa fa-bed"></i>五十三厅</li>
                                <li><i class="fa fa-tty"></i> 别墅花园</li>
                            </ul>
                        </div>
                        <div class="properties-content">
                            <h3><a href="#">香港东路22号</a></h3>
                            <p><i class="fa fa-map-marker"></i> 大学区, 市南区, 青岛</p>
                            <p class="detail-text">与海军博物馆一墙之隔，步行一分钟到达海边，拍照网红街大学路位于周边，可供游玩。 </p>
                            <div class="price-detail">
                                <p class="price-range pull-left">¥ 3,200,000</p>
                                <a href="#" class="price-detail pull-right">Details <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="properties-title margintop100">
                        <h2>近期房产</h2>
                        <a href="#" class="view-more">&nbsp;查看全部&nbsp;</a>
                    </div>
                </div>
            </div> 

            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="single-featured-properties">
                        <div class="properties-image">
                            <a href="#"><img src="image/properties/4.jpg" alt=""></a>
                        </div>
                        <div class="sale-tag">
                            <p>&nbsp;&nbsp;待售&nbsp;&nbsp;</p>
                        </div>
                        <div class="properties-include">
                            <ul>
                                <li><i class="fa fa-home"></i> 452 ㎡</li>
                                <li><i class="fa fa-bed"></i>三室一厅</li>
                                <li><i class="fa fa-tty"></i> 独立卫浴</li>
                            </ul>
                        </div>
                        <div class="properties-content">
                            <h3><a href="#">香港东路120号</a></h3>
                            <p><i class="fa fa-map-marker"></i> 大学区, 市南区, 青岛</p>
                            <p class="detail-text">与海军博物馆一墙之隔，步行一分钟到达海边，拍照网红街大学路位于周边，可供游玩。 </p>
                            <div class="price-detail">
                                <p class="price-range pull-left">¥ 3,200,000</p>
                                <a href="#" class="price-detail pull-right">Details <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="single-featured-properties">
                        <div class="properties-image">
                            <a href="#"><img src="image/properties/5.jpg" alt=""></a>
                        </div>
                        <div class="sale-tag">
                            <p>&nbsp;&nbsp;待售&nbsp;&nbsp;</p>
                        </div>
                        <div class="properties-include">
                            <ul>
                                <li><i class="fa fa-home"></i> 266 ㎡</li>
                                <li><i class="fa fa-bed"></i>五十一厅</li>
                                <li><i class="fa fa-tty"></i> 两间浴室</li>
                            </ul>
                        </div>
                        <div class="properties-content">
                            <h3><a href="#">银川东路23号</a></h3>
                            <p><i class="fa fa-map-marker"></i> 沿海, 市南区, 青岛</p>
                            <p class="detail-text">毗邻五四广场、奥帆中心、第二第三海水浴场，以及市政府、台东商业区等行政商业中心。 </p>
                            <div class="price-detail">
                                <p class="price-range pull-left">¥ 3,610,000</p>
                                <a href="#" class="price-detail pull-right">Details <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="single-featured-properties">
                        <div class="properties-image">
                            <a href="#"><img src="image/properties/6.jpg" alt=""></a>
                        </div>
                        <div class="sale-tag">
                            <p>For Sale</p>
                        </div>
                        <div class="properties-include">
                            <ul>
                                <li><i class="fa fa-home"></i> 30,000 Acres</li>
                                <li><i class="fa fa-bed"></i>4 Bedrooms</li>
                            </ul>
                        </div>
                        <div class="properties-content">
                            <h3><a href="#">South Mervin Boulevard</a></h3>
                            <p><i class="fa fa-map-marker"></i> Merrick Way, Miami, USA</p>
                            <p class="detail-text">Lorem ipsum dolor sit amet. Aenean commodo ligula eget dolor. </p>
                            <div class="price-detail">
                                <p class="price-range pull-left">$ 1,23,00.000</p>
                                <a href="#" class="price-detail pull-right">Details <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </section>

    <section class="footer-top-area  area-pading">
        <div class="container">
            <div class="row">
                <!-- Single Footer Widget-->
                <div class="col-md-4 col-sm-4">
                    <div class="single-footer-widget fix">
                        <div class="widget-title">
                            <h3>近期房源</h3>
                        </div>
                        <div class="single-property-footer-post">
                            <div class="properties-img pull-left">
                                <a href="#"><img src="image/widget/1.jpg" alt=""></a>
                                <p>&nbsp;&nbsp;待售&nbsp;&nbsp;</p>
                            </div>
                            <div class="properties-content-footer pull-left">
                                <a href="#"><h3>现代住宅</h3></a>
                                <p>公寓式</p>
                                <p class="property-price">¥1,256,000</p>
                            </div>
                        </div>
                        <div class="single-property-footer-post">
                            <div class="properties-img pull-left">
                                <a href="#"><img src="image/widget/2.jpg" alt=""></a>
                                <p>&nbsp;&nbsp;待售&nbsp;&nbsp;</p>
                            </div>
                            <div class="properties-content-footer pull-left">
                                <a href="#"><h3>现代公寓</h3></a>
                                <p>公寓式</p>
                                <p class="property-price">¥2,356,000</p>
                            </div>
                        </div>
                        <div class="single-property-footer-post">
                            <div class="properties-img pull-left">
                                <a href="#"><img src="image/widget/3.jpg" alt=""></a>
                                <p>&nbsp;&nbsp;待售&nbsp;&nbsp;</p>
                            </div>
                            <div class="properties-content-footer pull-left">
                                <a href="#"><h3>现代公寓</h3></a>
                                <p>公寓式</p>
                                <p class="property-price">¥1,452,000</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 hidden-sm">
                    <div class="single-footer-widget fix">
                        <div class="widget-title">
                            <h3>快捷链接</h3>
                        </div>
                        <div class="link-list">
                            <ul>
                                <li><a href="#">怎样添加房源</a></li>
                                <li><a href="#">条款及细则</a></li>
                                <li><a href="#">隐私政策 </a></li>
                                <li><a href="#">常遇到的问题</a></li>
                                <li><a href="#">称为代理</a></li>
                                <li><a href="#">联系我们！</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4">
                    <div class="single-footer-widget fix">
                        <div class="widget-title">
                            <h3>联系信息</h3>
                        </div>
                    </div>
                    <div class="address-wrap">
                        <i class="fa fa-map-marker"></i>
                        <div class="address-text">
                            <h4>联系地址:</h4>
                            <p>青岛 <br>市南区，宁夏路308号 <br>青岛大学滢园508公司. <br></p>
                        </div>
                    </div>
                    <div class="address-wrap">
                        <i class="fa fa-phone"></i>
                        <div class="address-text">
                            <h4>联系电话</h4>
                            <p>+ 123 456 789 000</p>
                        </div>
                    </div>
                    <div class="address-wrap">
                        <i class="fa fa-envelope"></i>
                        <div class="address-text">
                            <h4>电子邮件</h4>
                            <p>admin@QDUNiit.com</p>
                        </div>
                    </div>
                    <div class="footer-bookmark">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="copy-right-text">
                        <p> 版权所有 &copy;   王涵玉 汪星源 刘甜甜 魏琪晨 李春 2019-2020 人民当家作组制作</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="trams-poly pull-right">
                        <ul>
                            <li><a href="#">使用条款</a></li>
                            <li><a href="#">关于</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Main jQuery file -->
    <script src="js/jquery-1.11.3.min.js"></script>
    <!-- Bootstrap Js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Owl carousel js -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- scroll up js -->
    <script src="js/jquery.scrollUp.min.js"></script>
    <!-- BX Slider js -->
    <script src="js/jquery.bxslider.min.js"></script>
    <!-- wow js -->
    <script src="js/wow.min.js"></script>
    <!-- meanmenu js -->
    <script src="js/jquery.meanmenu.js"></script>
    <!-- Initialize WOW js for Animation-->
    <script>
            new WOW().init();
    </script>
    <!-- Theme jQuery Codes goes hear -->
    <script src="js/indexscript.js"></script>
    
    
    
    <!--        <div id="d1">
            <div id="d1_1">
                <img src="image/timg.jpg" style=" width: 1503px; height: 200px; z-index: 20;">
                <img id="img4" src="image/1.jpg">
                <br><br><br><br>
                <p style=" margin-left: 620px; margin-top: 20px; font-family: 黑体; font-size: 22px; color: #737373;">管理员姓名</p>
                <button style="border-radius: 5px; height: 40px; width: 120px; background-color: #5cb85c;
                        color: #fafafa; margin-left: 350px;margin-top: 20px;">
                    <a class="a1" href="#">管理用户</a>
                </button>
                <button style="border-radius: 5px; height: 40px; width: 120px; background-color: #EEE; 
                        color: #fafafa; margin-left: 50px;margin-top: 20px;">
                    <a class="a1" href="to_user">管理中介用户</a>
                </button>
                <button style="border-radius: 5px; height: 40px; width: 120px; background-color: #5cb85c; 
                        color: #fafafa; margin-left:50px;margin-top: 20px;">
                    <a class="a1" href="to_house">管理房源信息</a>
                </button>
                <button style=" border-radius: 5px; height: 40px; width: 120px; background-color: #EEE; 
                        color: #5cb85c; margin-left: 50px;margin-top: 20px;">
                    <a class="a1" href="#">返回主页</a>
                </button>
                <br><br>
            </div>
                <br>
        </div>-->
    </body>
</html>
