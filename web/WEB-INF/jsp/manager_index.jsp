<%-- 
    Document   : manager_index
    Created on : 2019-12-16, 14:42:38
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>管理员管理后台界面</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="layui/css/layui.css">
    </head>
    <body class="layui-layout-body">
        <div class="layui-layout layui-layout-admin">
            <div class="layui-header">
                <div class="layui-logo">管理员后台管理界面</div>
                <!-- 头部区域（可配合layui已有的水平导航） -->
                <ul class="layui-nav layui-layout-left">
                    <li class="layui-nav-item"><a href="manager/to_house">房屋管理</a></li>
                    <li class="layui-nav-item"><a href="manager/to_user">中介管理</a></li>
                    <li class="layui-nav-item"><a href="manager/to_webuser">用户管理</a></li>
                </ul>
                <ul class="layui-nav layui-layout-right">
                    <li class="layui-nav-item">
                        <a href="javascript:;">
                            <img src="image/1.jpg" class="layui-nav-img">
                            管理员
                        </a>
                    </li>
                    <li class="layui-nav-item"><a href="#">退出</a></li>
                </ul>
            </div>

            <div class="layui-side layui-bg-black">
                <div class="layui-side-scroll">
                    <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                    <ul class="layui-nav layui-nav-tree">
                        <li class="layui-nav-item"><a class="layui-this" href="index">首页</a></li>
                        <li class="layui-nav-item">
                            <a href="manager/to_webuser">用户管理</a>
                            <a href="manager/to_user">中介管理</a>
                            <a href="manager/to_house">房源信息管理</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="layui-body">
                <!-- 内容主体区域 -->
                <div style="padding: 15px;">
                    <fieldset class="layui-elem-field">
                        <legend>控制台-现有管理员信息</legend>
                        <div class="layui-field-box">
                            <table class="layui-table">
                                <colgroup>
                                    <col width="150">
                                    <col width="200">
                                    <col>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>昵称</th>
                                        <th>加入时间</th>
                                        <th>管理权限</th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Abby</td>
                                        <td>2016-11-29</td>
                                        <td>管理权限级别 : A</td>
                                    </tr>
                                    <tr>
                                        <td>Star</td>
                                        <td>2018-11-28</td>
                                        <td>管理权限级别 : A</td>
                                    </tr>
                                    <tr>
                                        <td>sentsin</td>
                                        <td>2019-01-27</td>
                                        <td>管理权限级别 : B</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="layui-footer">
                <!-- 底部固定区域 -->
                © 版权所有 王涵玉 汪星源 刘甜甜 魏琪晨 李春 - 2019-2020 - 人民当家作组制作
            </div>
        </div>
        <script type="text/javascript" src="layui/js/jquery.min.js"></script>
        <script type="text/javascript" src="layui/js/layui.js"></script>
        <script type="text/javascript" src="layui/js/index.js"></script>
    </body>
</html>
