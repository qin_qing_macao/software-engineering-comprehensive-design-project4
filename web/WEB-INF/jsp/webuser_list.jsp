<%-- 
    Document   : webuser_list
    Created on : 2019-12-17, 9:35:27
    Author     : dell
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>管理用户</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="layui/css/layui.css">
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/script_webUser.js"></script>
    </head>
        <body class="layui-layout-body">
        <div class="layui-layout layui-layout-admin">
            <div class="layui-header">
                <div class="layui-logo">管理员后台管理界面</div>
                <!-- 头部区域（可配合layui已有的水平导航） -->
                <ul class="layui-nav layui-layout-left">
                    <li class="layui-nav-item"><a href="manager/to_house">房屋管理</a></li>
                    <li class="layui-nav-item"><a href="manager/to_user">中介管理</a></li>
                    <li class="layui-nav-item"><a href="manager/to_webuser">用户管理</a></li>
                </ul>
                <ul class="layui-nav layui-layout-right">
                    <li class="layui-nav-item">
                        <a href="javascript:;">
                            <img src="image/1.jpg" class="layui-nav-img">
                            管理员
                        </a>
                    </li>
                    <li class="layui-nav-item"><a href="#">退出</a></li>
                </ul>
            </div>

            <div class="layui-side layui-bg-black">
                <div class="layui-side-scroll">
                    <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                    <ul class="layui-nav layui-nav-tree">
                        <li class="layui-nav-item"><a class="layui-this" href="index">首页</a></li>
                        <li class="layui-nav-item">
                            <a href="manager/to_webuser">用户管理</a>
                            <a href="manager/to_user">中介管理</a>
                            <a href="manager/to_house">房源信息管理</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--<div class="layui-body">-->
                <!-- 内容主体区域 -->
                <div style="padding-top:30px; padding-left:205px; ">
                    <fieldset class="layui-elem-field">
                        <legend>网站用户管理</legend>
                        <div class="layui-field-box">
                            <div class="container text-center">
                                <br>
                                <h1 class="text-warning">管理网站用户</h1>
                                <hr>
                                <a href="javascript:findAll();" class="btn btn-info">查询所有网站用户</a>
                                &nbsp;&nbsp;
                                <a href="javascript:popupAdd();" class="btn btn-warning">+添加新用户</a>
                                &nbsp;&nbsp;
                                <a href="index" class="btn btn-success"><<返回首页</a>
                                <br>
                                <hr>
                                <table id="dataTable" class="table table-hover table-striped">
                                    <tr>
                                        <th>用户编号</th>
                                        <th>头像</th>
                                        <th>用户姓名</th>
                                        <th>电话</th>
                                        <th>邮箱</th>
                                        <th>生日</th>
                                        <th>性别</th>
                                        <th>密码</th>
                                        <th>操作</th>
                                    </tr>
                                    <c:forEach items="${userList}" var="w">
                                        <tr id="tr${w.wId}">
                                            <td>${u.uId}</td>
                                            <td><img class="img-circle" src="image/${w.wId}.jpg" height="36" width="36" alt="用户头像"></td>
                                            <td>${w.wName}</td>
                                            <td>${w.wPhone}</td>
                                            <td>${w.wEmail}</td>
                                            <td>${w.wBirthday}</td>
                                            <td>${w.wSex}</td>
                                            <td>${w.wPwd}</td>
                                        </tr>
                                    </c:forEach>
                                </table>
                                <br>
                                <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <br><br><br>
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h2 class="modal-title text-danger" id="editModalLabel">编辑用户信息</h2>
                                            </div>
                                            <div class="modal-body">
                                                <form id="editForm" class="form-inline" method="post">
                                                    <span>用户编号：</span>
                                                    <input type="text" id="inputId" name="wId" class="form-control" readonly>
                                                    <br><br>
                                                    <span>用户头像：</span>
                                                    <input type="text" id="inputImage" name="wImage" class="form-control">
                                                    <br><br>
                                                    <span>用户姓名：</span>
                                                    <input type="text" id="inputName" name="wName" class="form-control">
                                                    <br><br>
                                                    <span>用户电话：</span>
                                                    <input type="text" id="inputPhone" name="wPhone" class="form-control">
                                                    <br><br>
                                                    <span>用户邮箱：</span>
                                                    <input type="text" id="inputEmail" name="wEmail" class="form-control">   
                                                    <br><br>
                                                    <span>生日：</span>
                                                    <input type="text" id="inputBirthday" name="wBirthday" class="form-control">
                                                    <br><br>
                                                    <span>性别：</span>
                                                    <input type="text" id="inputSex" name="wSex" class="form-control">
                                                    <br><br>
                                                    <span>密码：</span>
                                                    <input type="text" id="inputPwd" name="wPwd" class="form-control">
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                                <button id="btnEdit" type="button" class="btn btn-primary">保存修改</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <br><br><br>
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h2 class="modal-title text-danger" id="addModalLabel">添加新用户</h2>
                                            </div>
                                            <div class="modal-body">
                                                <form id="addForm" class="form-inline" method="post">
                                                    <span>用户头像：</span>
                                                    <input type="text" id="usImage" name="wImage" class="form-control" placeholder="请上传用户头像">
                                                    <br><br>
                                                    <span>用户姓名：</span>
                                                    <input type="text" id="usName" name="wName" class="form-control" placeholder="请输入用户姓名">
                                                    <br><br>
                                                    <span>电话：</span>
                                                    <input type="text" id="usPhone" name="wPhone" class="form-control" placeholder="请输入用户电话">   
                                                    <br><br>
                                                    <span>邮箱：</span>
                                                    <input type="text" id="usEmail" name="wEmail" class="form-control" placeholder="请输入用户邮箱">
                                                    <br><br>
                                                    <span>生日：</span>
                                                    <input type="text" id="usBirthday" name="wBirthday" class="form-control" placeholder="请输入用户生日">
                                                    <br><br>
                                                    <span>性别：</span>
                                                    <input type="text" id="usSex" name="wSex" class="form-control" placeholder="请输入用户性别">
                                                    <br><br>
                                                    <span>密码：</span>
                                                    <input type="text" id="usPwd" name="uPwd" class="form-control" placeholder="请输入用户密码">
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                                <button id="btnAdd" type="button" class="btn btn-danger">添加</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            <!--</div>-->

            <div class="layui-footer">
                <!-- 底部固定区域 -->
                © 版权所有 王涵玉 汪星源 刘甜甜 魏琪晨 李春 - 2019-2020 - 人民当家作组制作
            </div>
        </div>
    </body>
</html>
