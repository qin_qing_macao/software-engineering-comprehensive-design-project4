<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>管理房源信息</title>
        <base href="${pageContext.request.contextPath}/">
        <link rel="stylesheet" href="layui/css/layui.css">
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/style.css"/>
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/script.js"></script>
    </head>
    <body class="layui-layout-body">
        <div class="layui-layout layui-layout-admin">
            <div class="layui-header">
                <div class="layui-logo">管理员后台管理界面</div>
                <!-- 头部区域（可配合layui已有的水平导航） -->
                <ul class="layui-nav layui-layout-left">
                    <li class="layui-nav-item"><a href="manager/to_house">房屋管理</a></li>
                    <li class="layui-nav-item"><a href="manager/to_user">中介管理</a></li>
                    <li class="layui-nav-item"><a href="manager/to_webuser">用户管理</a></li>
                </ul>
                <ul class="layui-nav layui-layout-right">
                    <li class="layui-nav-item">
                        <a href="javascript:;">
                            <img src="image/1.jpg" class="layui-nav-img">
                            管理员
                        </a>
                    </li>
                    <li class="layui-nav-item"><a href="#">退出</a></li>
                </ul>
            </div>

            <div class="layui-side layui-bg-black">
                <div class="layui-side-scroll">
                    <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                    <ul class="layui-nav layui-nav-tree">
                        <li class="layui-nav-item"><a class="layui-this" href="index">首页</a></li>
                        <li class="layui-nav-item">
                            <a href="manager/to_webuser">用户管理</a>
                            <a href="manager/to_user">中介管理</a>
                            <a href="manager/to_house">房源信息管理</a>
                        </li>
                    </ul>
                </div>
            </div>

            <!--<div class="layui-body">-->
                <!-- 内容主体区域 -->
                <div style="padding-top:30px; padding-left:205px;">
                    <fieldset class="layui-elem-field">
                        <legend>管理房源信息</legend>
                        <div class="layui-field-box">
                            <div class="container text-center">
                                <br>
                                <h1 class="text-warning">管理中介用户</h1>
                                <hr>
                                <a href="javascript:findall();" class="btn btn-info">查询所有房屋信息</a>
                            </div>
                            &nbsp;&nbsp;
                            <div class="container text-center"><br>
                                <form action="house/selectHouseByKeyword" class="form-inline" >
                                    <input type="text" id="kwd" name="keyword" class="form-control" placeholder="请输入检索信息">
                                    <button class="btn btn-sm btn-primary">查询</button>
                                </form>
                            </div>
                            <br>
                            <hr>
                            <table id="dataTable1" class="table table-hover table-striped">
                                <c:forEach items="${houseList}" var="h">
                                    <div class='datarow img-thumbnail' style='border: solid #ced9dc; margin: 16px;padding: 20px;text-align:center;width:250px;height:350px;display: inline-block'>
                                        <img src="image/${h.uId}.jpg" style="width: 90px;height: 90px;"><br><hr>
                                        <div style='padding:3px;width:210px;text-align:left;height: 300px;display: inline-block'>
                                            房屋编号：${h.hid}
                                            <br>中介编号：${h.uId}
                                            <br>房屋价格：${h.price}
                                            <br>房屋位置：${h.location}
                                            <br>房屋类型：${h.houseType}
                                            <br>房屋面积：${h.area}
                                            <br>详细地址：${h.address}
                                            <!--<br>房屋图片：${h.hpic}-->
                                        </div> 
                                    </div> 
                                </c:forEach>
                            </table>
                            <br>
                            <div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="editModalLabe">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h2 class="modal-title text-danger" id="editModalLabe">编辑房屋信息</h2>
                                        </div>
                                        <div class="modal-body">
                                            <form id="editForm1" class="form-inline" method="post">
                                                <span>房屋编号：</span>
                                                <input type="text" id="inputHid" name="hid" class="form-control" readonly>
                                                <br><br>
                                                <span>价格：</span>
                                                <input type="text" id="inputHprice" name="price" class="form-control">
                                                <br><br>
                                                <span>位置：</span>
                                                <input type="text" id="inputHlocation" name="location" class="form-control">
                                                <br><br>
                                                <span>房屋类型：</span>
                                                <input type="text" id="inputHhouseType" name="houseType" class="form-control">
                                                <br><br>
                                                <span>面积：</span>
                                                <input type="text" id="inputHarea" name="area" class="form-control">   
                                                <br><br>
                                                <span>详细地址：</span>
                                                <input type="text" id="inputHaddress" name="address" class="form-control">
                                                <br><br>
                                                <span>房屋图片：</span>
                                                <input type="text" id="inputHhpic" name="hpic" class="form-control">
                                                <br><br>
                                                <span>中介编号：</span>
                                                <input type="text" id="inputUid" name="uId" class="form-control" readonly>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                            <button id="btnEdit1" type="button" class="btn btn-primary">保存修改</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            <!--</div>-->

            <div class="layui-footer">
                <!-- 底部固定区域 -->
                © 版权所有 王涵玉 汪星源 刘甜甜 魏琪晨 李春 - 2019-2020 - 人民当家作组制作
            </div>
        </div>
    </body>
</html>
