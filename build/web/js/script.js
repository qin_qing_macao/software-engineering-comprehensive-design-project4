
var currentuId;
//var currenthId;

function findAll() {
    $.ajax({
        url: 'user/findAll',
        type: "POST",
        success: function (list) {

            $("#dataTable tr.datarow").remove();

            $.each(list, function (index, u) {
                var str = "<tr id=tr" + u.uId + " class=\"datarow\">" +
                        "<td>" + u.uId + "</td>" +
                        "<td>" + u.uName + "</td>" +
                        "<td>" + u.uPhone + "</td>" +
                        "<td>" + u.uEmail + "</td>" +
                        "<td>" + u.uBirthday + "</td>" +
                        "<td>" + u.uSex + "</td>" +
                        "<td>" + u.uPwd + "</td>" +
                        "<td>" +"<img src='image/" +u.uId+ "'style='width: 30px;height: 30px;'>"+ "</td>" +
//                        "<td>" +u.uImage + "</td>" +
                        "<td>" +
                        "<a href=\"javascript:popupEdit(" + u.uId + ");\">编辑</a>&nbsp;&nbsp;" +
                        "<a href=\"javascript:delete_user(" + u.uId + ");\">删除</a>" +
                        "</td></tr>";

                $("#dataTable").append(str);
            });
        },
        
        error: function (req, status, error) {
            alert("Ajax请求失败！" + error);
        }
    });
}

function findall() {
    $.ajax({
        url: 'house/findall',
        type: "POST",
        success: function (list) {
            $("#dataTable1 tr.datarow").remove();
            $.each(list, function (index, h) {
                
            var str = "<div class='datarow img-thumbnail' style='border: solid #ced9dc; margin: 16px;padding: 20px;text-align:center;width:250px;height: 400px;display: inline-block'>"
                                +" <div>"
                                +"<img src='image/"+ h.uid +".jpg' style='width: 200px;height: 100px;'>"
                                +"<br><hr>"
                                +" <div style='padding:3px;width:210px;text-align:left;height: 300px;display: inline-block'>"
                                +"房屋编号："+h.hid
                                +"<br>中介编号："+h.uId
                                +"<br>房屋价格："+h.price
                                +"<br>房屋位置："+h.location
                                +"<br>房屋类型："+h.houseType
                                +"<br>房屋面积："+h.area
                                +"<br>详细地址："+h.address
                                +"<br>房屋图片："+h.hpic
                                +"<br>&nbsp;&nbsp;<a href=\"javascript:houseEdit('" + h.hid + "');\">编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                                +"<a href=\"javascript:delete_house('" + h.hid + "');\">删除</a>" 
                                +"</div></div></div>";

                $("#dataTable1").append(str);
            });
        },
        
        error: function (req, status, error) {
            alert("Ajax请求失败！" + error);
        }
    });
}

function popupEdit(uid) {

    currentuId = uid;
    $.ajax({
        url: 'user/get_user_to_edit',
        type: 'POST',
        data: {useId: uid}, 
        success: function (us) {
            $("#inputId").val(us.uId);
            $("#inputName").val(us.uName);
            $("#inputPhone").val(us.uPhone);
            $("#inputEmail").val(us.uEmail);
            $("#inputBirthday").val(us.uBirthday);
            $("#inputSex").val(us.uSex);
            $("#inputPwd").val(us.uPwd);
            $("#inputImage").val(us.uImage);
            $('#editModal').modal('show');
        },
        error: function (req, status, error) {
            alert("Ajax请求失败，错误：" + error);
        }
    });
}

function houseEdit(hid) {

    $.ajax({
        url:'house/get_house_to_edit',
        type:'POST',
        data:{"hid": hid}, //发送一个请求参数，参数名为houseId，参数值为传入的hid变量的值
        success: function (hs) {
            $("#inputHid").val(hs.hid);
            $("#inputUid").val(hs.uId);
            $("#inputHprice").val(hs.price);
            $("#inputHlocation").val(hs.location);
            $("#inputHhouseType").val(hs.houseType);
            $("#inputHarea").val(hs.area);
            $("#inputHaddress").val(hs.address);
            $("#inputHhpic").val(hs.hpic);
            $('#editModal1').modal('show');
        },
        error: function (req, status, error) {
            alert("Ajax请求失败，错误：" + error);
        }
    });
}

function popupAdd() {
    $('#addModal').modal('show');
    $("#addForm")[0].reset();
}

//定义一个函数,根据编号删除用户
function delete_user(uid) {
    $.ajax({
        url: 'user/delete',
        type: 'POST',
        data: {useId: uid}, 
        success: function () {
            //如果能执行到success，说明后台删除成功，这里同时将表格中对应的数据行删除
            $("#tr" + uid).remove(); // remove是删除当前元素和其内容
        },
        error: function (req, status, error) {
            alert("Ajax请求失败，错误：" + error);
        }
    });
}

//定义一个函数,根据房屋编号删除房屋信息
function delete_house(hid) {
    $.ajax({
        url: 'house/delete_house',
        type: 'POST',
        data: {"hid": hid}, //发送一个请求参数，参数名为houseId，参数值为传入的hid变量的值
        success: function () {
            //如果能执行到success，说明后台删除成功，这里同时将表格中对应的数据行删除
            $("#tr" + hid).remove(); // remove是删除当前元素和其内容
        },
        error: function (req, status, error) {
            alert("Ajax请求失败，错误：" + error);
        }
    });
}

$(document).ready(function () {

    $("#btnEdit").click(function () {
        $.ajax({
            url: 'user/edit_user',
            type: 'POST',
            data: $("#editForm").serialize(),
            success: function () {
                $("#tr" + currentuId).children().eq(1).html($("#inputName").val());
                $("#tr" + currentuId).children().eq(2).html($("#inputPhone").val());
                $("#tr" + currentuId).children().eq(3).html($("#inputEmail").val());
                $("#tr" + currentuId).children().eq(4).html($("#inputBirthday").val());
                $("#tr" + currentuId).children().eq(5).html($("#inputSex").val());
                $("#tr" + currentuId).children().eq(6).html($("#inputPwd").val());
                $("#tr" + currentuId).children().eq(7).html($("#inputImage").val());
                $('#editModal').modal('hide');
            },
            error: function (req, status, error) {
                alert("Ajax请求失败，错误：" + error);
            }
        });
    });
 
    $("#btnAdd").click(function () {
        $.ajax({
            url: 'user/add_user',
            type: 'POST',
            data: $("#addForm").serialize(),
            success: function (useId) {
                var u = {
                    uId: useId,
                    uName: $("#usName").val(),
                    uPhone: $("#usPhone").val(),
                    uEmail: $("#usEmail").val(),
                    uBirthday: $("#usBirthday").val(),
                    uSex: $("#usSex").val(),
                    uPwd: $("#usPwd").val(),
                    uImage: $("#usImage").val()
                }; 

                var str = "<tr id=tr" + u.uId + " class=\"datarow\">" +
                        "<td>" + u.uId + "</td>" +
                        "<td>" + u.uName + "</td>" +
                        "<td>" + u.uPhone + "</td>" +
                        "<td>" + u.uEmail + "</td>" +
                        "<td>" + u.uBirthday + "</td>" +
                        "<td>" + u.uSex + "</td>" +
                        "<td>" + u.uPwd + "</td>" +
                        "<td>" + u.uImage + "</td>" +
                        "<td>" +
                        "<a href=\"javascript:popupEdit(" + u.uId + ");\">编辑</a>&nbsp;&nbsp;" +
                        "<a href=\"javascript:delete_user(" + u.uId + ");\">删除</a>" +
                        "</td></tr>";

                $("#dataTable").append(str);

                $("#addModal").modal("hide");
            },
            error: function (req, status, error) {
                alert("Ajax请求失败，错误：" + error);
            }
        });
    });    
});

$(document).ready(function () {

    $("#btnEdit1").click(function () {
        $.ajax({
            url: 'house/edit_house',
            type: 'POST',
            data: $("#editForm1").serialize(),
            success: function () {
                $("#tr" + currentuId).children().eq(1).html($("#inputUid").val());
                $("#tr" + currentuId).children().eq(2).html($("#inputHprice").val());
                $("#tr" + currentuId).children().eq(3).html($("#inputHlocation").val());
                $("#tr" + currentuId).children().eq(4).html($("#inputHhouseType").val());
                $("#tr" + currentuId).children().eq(5).html($("#inputHarea").val());
                $("#tr" + currentuId).children().eq(6).html($("#inputHaddress").val());
                $("#tr" + currentuId).children().eq(7).html($("#inputHhpic").val());
                $('#editModal1').modal('hide');
            },   
             error: function (req, status, error) {
                alert("Ajax请求失败，错误：" + error);
            }
        });
    });
});
