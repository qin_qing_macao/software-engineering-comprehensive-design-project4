
var currentuId;
//var currenthId;

function findAll() {
    $.ajax({
        url: 'webuser/findAll',
        type: "POST",
        success: function (list) {

            $("#dataTable tr.datarow").remove();

            $.each(list, function (index, w) {
                var str = "<tr id=tr" + w.wId + " class=\"datarow\">" +
                        "<td>" + w.wId + "</td>" +
                        "<td>" + w.wName + "</td>" +
                        "<td>" + w.wPhone + "</td>" +
                        "<td>" + w.wEmail + "</td>" +
                        "<td>" + w.wBirthday + "</td>" +
                        "<td>" + w.wSex + "</td>" +
                        "<td>" + w.wPwd + "</td>" +
                        "<td>" +"<img src='image/" +w.wId+ "'style='width: 30px;height: 30px;'>"+ "</td>" +
//                        "<td>" +u.uImage + "</td>" +
                        "<td>" +
                        "<a href=\"javascript:popupEdit(" + w.wId + ");\">编辑</a>&nbsp;&nbsp;" +
                        "<a href=\"javascript:delete_user(" + w.wId + ");\">删除</a>" +
                        "</td></tr>";

                $("#dataTable").append(str);
            });
        },
        
        error: function (req, status, error) {
            alert("Ajax请求失败！" + error);
        }
    });
}


function popupEdit(wid) {

    currentuId = wid;
    $.ajax({
        url: 'webuser/get_user_to_edit',
        type: 'POST',
        data: {useId: wid}, 
        success: function (us) {
            $("#inputId").val(us.wId);
            $("#inputName").val(us.wName);
            $("#inputPhone").val(us.wPhone);
            $("#inputEmail").val(us.wEmail);
            $("#inputBirthday").val(us.wBirthday);
            $("#inputSex").val(us.wSex);
            $("#inputPwd").val(us.wPwd);
            $("#inputImage").val(us.wImage);
            $('#editModal').modal('show');
        },
        error: function (req, status, error) {
            alert("Ajax请求失败，错误：" + error);
        }
    });
}

function popupAdd() {
    $('#addModal').modal('show');
    $("#addForm")[0].reset();
}

//定义一个函数,根据编号删除用户
function delete_user(wid) {
    $.ajax({
        url: 'webuser/delete',
        type: 'POST',
        data: {useId: wid}, 
        success: function () {
            //如果能执行到success，说明后台删除成功，这里同时将表格中对应的数据行删除
            $("#tr" + wid).remove(); // remove是删除当前元素和其内容
        },
        error: function (req, status, error) {
            alert("Ajax请求失败，错误：" + error);
        }
    });
}

$(document).ready(function () {

    $("#btnEdit").click(function () {
        $.ajax({
            url: 'webuser/edit_user',
            type: 'POST',
            data: $("#editForm").serialize(),
            success: function () {
                $("#tr" + currentuId).children().eq(1).html($("#inputName").val());
                $("#tr" + currentuId).children().eq(2).html($("#inputPhone").val());
                $("#tr" + currentuId).children().eq(3).html($("#inputEmail").val());
                $("#tr" + currentuId).children().eq(4).html($("#inputBirthday").val());
                $("#tr" + currentuId).children().eq(5).html($("#inputSex").val());
                $("#tr" + currentuId).children().eq(6).html($("#inputPwd").val());
                $("#tr" + currentuId).children().eq(7).html($("#inputImage").val());
                $('#editModal').modal('hide');
            },
            error: function (req, status, error) {
                alert("Ajax请求失败，错误：" + error);
            }
        });
    });
 
    $("#btnAdd").click(function () {
        $.ajax({
            url: 'webuser/add_user',
            type: 'POST',
            data: $("#addForm").serialize(),
            success: function (useId) {
                var w = {
                    wId: useId,
                    wName: $("#usName").val(),
                    wPhone: $("#usPhone").val(),
                    wEmail: $("#usEmail").val(),
                    wBirthday: $("#usBirthday").val(),
                    wSex: $("#usSex").val(),
                    wPwd: $("#usPwd").val(),
                    wImage: $("#usImage").val()
                }; 

                var str = "<tr id=tr" + w.wId + " class=\"datarow\">" +
                        "<td>" + w.wId + "</td>" +
                        "<td>" + w.wName + "</td>" +
                        "<td>" + w.wPhone + "</td>" +
                        "<td>" + w.wEmail + "</td>" +
                        "<td>" + w.wBirthday + "</td>" +
                        "<td>" + w.wSex + "</td>" +
                        "<td>" + w.wPwd + "</td>" +
                        "<td>" + w.wImage + "</td>" +
                        "<td>" +
                        "<a href=\"javascript:popupEdit(" + w.wId + ");\">编辑</a>&nbsp;&nbsp;" +
                        "<a href=\"javascript:delete_user(" + w.wId + ");\">删除</a>" +
                        "</td></tr>";

                $("#dataTable").append(str);

                $("#addModal").modal("hide");
            },
            error: function (req, status, error) {
                alert("Ajax请求失败，错误：" + error);
            }
        });
    });    
});
