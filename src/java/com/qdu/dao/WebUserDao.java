/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qdu.dao;

import com.qdu.pojo.WebUsers;
import java.util.List;

/**
 *
 * @author dell
 */
public interface WebUserDao {
    Object insert(WebUsers webuser);
    
    void update(WebUsers webuser);

    void deleteByWId(Integer id);

    WebUsers getOneByWId(Integer id);

    List<WebUsers> getAll();
    
}
