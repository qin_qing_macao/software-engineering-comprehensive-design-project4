package com.qdu.dao;

import com.qdu.pojo.IntermediaryUsers;
import java.util.List;

public interface UserDao {
    Object insert(IntermediaryUsers user);
    
    void update(IntermediaryUsers user);

    void deleteByUId(Integer id);

    IntermediaryUsers getOneByUId(Integer id);

    List<IntermediaryUsers> getAll();
    
    List<IntermediaryUsers> getPartUserByKeyword(String keyword);
}
