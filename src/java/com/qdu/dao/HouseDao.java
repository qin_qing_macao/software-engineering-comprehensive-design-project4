package com.qdu.dao;

import com.qdu.pojo.HouseImfo;
import com.qdu.pojo.IntermediaryUsers;
import java.util.List;

public interface HouseDao {
    
    void update(HouseImfo house);

    void deleteByHId(String id);

    List<HouseImfo> getAll();
    
    List<HouseImfo> getPartHouseByKeyword(String keyword);
    
    HouseImfo getOneByUId(String id);
}
