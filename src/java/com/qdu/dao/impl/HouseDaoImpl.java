package com.qdu.dao.impl;

import com.qdu.dao.HouseDao;
import com.qdu.pojo.HouseImfo;
import com.qdu.pojo.IntermediaryUsers;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Repository
public class HouseDaoImpl implements HouseDao{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void update(HouseImfo house) {
        sessionFactory.getCurrentSession().update(house);
    }

    @Override
    public List<HouseImfo> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from HouseImfo").list();
    }

    @Override
    public List<HouseImfo> getPartHouseByKeyword(String keyword) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(HouseImfo.class);  
        criteria.add(Restrictions.like("keyword", "%" + keyword + "%"));  
        return criteria.list();
    }

    @Override
    public void deleteByHId(String id) {
        sessionFactory.getCurrentSession().delete(new HouseImfo(id));
    }  

    @Override
    public HouseImfo getOneByUId(String id) {
        return sessionFactory.getCurrentSession().get(HouseImfo.class, id);
    }
}
