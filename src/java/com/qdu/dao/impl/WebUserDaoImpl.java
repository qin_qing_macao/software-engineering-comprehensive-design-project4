package com.qdu.dao.impl;

import com.qdu.pojo.WebUsers;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.qdu.dao.WebUserDao;

@Repository
public class WebUserDaoImpl implements WebUserDao{
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Object insert(WebUsers webuser) { 
        return sessionFactory.getCurrentSession().save(webuser);
    }
    
    @Override
    public void update(WebUsers webuser) {
        sessionFactory.getCurrentSession().update(webuser);
    }
    
    @Override
    public void deleteByWId(Integer id) {
        sessionFactory.getCurrentSession().delete(new WebUsers(id));
    }
    @Override
    public WebUsers getOneByWId(Integer id) {
        return sessionFactory.getCurrentSession().get(WebUsers.class, id);
    }

    @Override
    public List<WebUsers> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from WebUsers").list();
    }

}

