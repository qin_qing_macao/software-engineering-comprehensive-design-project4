package com.qdu.dao.impl;

import com.qdu.pojo.IntermediaryUsers;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.qdu.dao.UserDao;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

@Repository
public class UserDaoImpl implements UserDao{
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Object insert(IntermediaryUsers user) { 
        return sessionFactory.getCurrentSession().save(user);
    }
    
    @Override
    public void update(IntermediaryUsers user) {
        sessionFactory.getCurrentSession().update(user);
    }
    
    @Override
    public void deleteByUId(Integer id) {
        sessionFactory.getCurrentSession().delete(new IntermediaryUsers(id));
    }
    @Override
    public IntermediaryUsers getOneByUId(Integer id) {
        return sessionFactory.getCurrentSession().get(IntermediaryUsers.class, id);
    }

    @Override
    public List<IntermediaryUsers> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from IntermediaryUsers").list();
    }

    @Override
    public List<IntermediaryUsers> getPartUserByKeyword(String keyword) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(IntermediaryUsers.class);  
        criteria.add(Restrictions.like("keyword", "%" + keyword + "%"));  
        return criteria.list();
    }
}

