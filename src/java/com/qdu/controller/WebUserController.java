/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qdu.controller;

import com.qdu.pojo.WebUsers;
import com.qdu.service.WebUserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dell
 */
@Controller
@RequestMapping("/webuser")
public class WebUserController {
    @Autowired
    private WebUserService webuserService;
  
    @PostMapping("/findAll")
    @ResponseBody
    public List findAll() {
        return webuserService.getWebUserList();
    }

    @PostMapping("/add_user")
    @ResponseBody
    public Integer addUser(WebUsers webuser) {
        return (Integer)webuserService.addWebUser(webuser);
    }
     
    @PostMapping("/get_user_to_edit")
    @ResponseBody
    public WebUsers getUserToEdit(Integer useId) {
        return webuserService.getWebUserByWId(useId);
    }

    @PostMapping("/edit_user")
    @ResponseBody
    public void editUser(WebUsers webuser) {
        webuserService.updateWebUser(webuser);
    }
 
    @PostMapping("/delete")
    @ResponseBody
    public void delete(Integer useId) {
        webuserService.deleteWebUser(useId);
    }
}
