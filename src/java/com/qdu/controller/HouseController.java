package com.qdu.controller;

import com.qdu.pojo.HouseImfo;
import com.qdu.service.HouseService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/house")
public class HouseController {
    @Autowired
    private HouseService houseService;
    
    @PostMapping("/findall")
    @ResponseBody
    public List findall() {
        return houseService.getHouseList();
    }

    @PostMapping("/get_house_to_edit")
    @ResponseBody
    public HouseImfo getHouseToEdit(String hid) {
        return houseService.getHouseByUId(hid);
    }

    @PostMapping("/edit_house")
    @ResponseBody
    public void editHouse(HouseImfo house) {
        houseService.updateHouse(house);
    }
 
    @PostMapping("/delete_house")
    @ResponseBody
    public void delete(String hid) {
        houseService.deleteHouse(hid);
    }
    @GetMapping("/selectHouseByKeyword")
    public String findHouseByKeyword(@RequestParam("keyword") String keyword, Model model) {
            model.addAttribute("houseList",houseService.getUserListBykeyword(keyword));
             return "house_list";
    }
}
