package com.qdu.controller;

import com.qdu.pojo.IntermediaryUsers;
import com.qdu.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
  
    @PostMapping("/findAll")
    @ResponseBody
    public List findAll() {
        return userService.getUserList();
    }

    @PostMapping("/add_user")
    @ResponseBody
    public Integer addUser(IntermediaryUsers user) {
        return (Integer)userService.addUser(user);
    }
     
    @PostMapping("/get_user_to_edit")
    @ResponseBody
    public IntermediaryUsers getUserToEdit(Integer useId) {
        return userService.getUserByUId(useId);
    }

    @PostMapping("/edit_user")
    @ResponseBody
    public void editUser(IntermediaryUsers user) {
        userService.updateUser(user);
    }
 
    @PostMapping("/delete")
    @ResponseBody
    public void delete(Integer useId) {
        userService.deleteUser(useId);
    }
    @GetMapping("/selectUserByKeyword")
    public String findUserByKeyword(@RequestParam("keyword") String keyword, Model model) {
            model.addAttribute("userList",userService.getUserListBykeyword(keyword));
             return "user_list";
    }
}
