package com.qdu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping({"/index", "/"})
    public String index() {
        return "index";
    }
    @RequestMapping("/manager_index")
    public String manager(){
        return "manager_index";
    }
//    @RequestMapping("/to_user")
//    public String to_user() {
//        return "user_list";
//    }
//    @RequestMapping("/to_house")
//    public String to_house() {
//        return "house_list";
//    }
}
