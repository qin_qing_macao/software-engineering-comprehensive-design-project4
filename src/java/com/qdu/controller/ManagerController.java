/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qdu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author dell
 */
@Controller
@RequestMapping("/manager")
public class ManagerController {
   
//    @RequestMapping("/manager_index")
//    public String index() {
//        return "manager_index";
//    }
//    @RequestMapping({"/index", "/"})
//    public String manager(){
//        return "index";
//    }
    @RequestMapping("/to_user")
    public String to_user() {
        return "user_list";
    }
    @RequestMapping("/to_house")
    public String to_house() {
        return "house_list";
    }
    @RequestMapping("/to_webuser")
    public String to_webuser(){
        return "webuser_list";
    }
    
}
