package com.qdu.service;

import com.qdu.pojo.IntermediaryUsers;
import java.util.List;

public interface UserService {
    
    //根据用户编号获得一个用户的全部信息
    IntermediaryUsers getUserByUId(Integer id);
    
    //获取所有用户列表
    List getUserList();

    //添加一个新用户
    Object addUser(IntermediaryUsers user);

    //更新用户信息
    void updateUser(IntermediaryUsers user);

    //根据用户姓名删除一个用户
    void deleteUser(Integer id);
    
    //根据关键字检索中介用户信息
    List getUserListBykeyword(String keyword);
}
