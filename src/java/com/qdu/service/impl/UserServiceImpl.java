package com.qdu.service.impl;

import com.qdu.pojo.IntermediaryUsers;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.qdu.dao.UserDao;
import com.qdu.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService{
    
    @Autowired
    private UserDao userDao;

//    @Transactional(readOnly = true)
    @Override
    public IntermediaryUsers getUserByUId(Integer id) {
        return userDao.getOneByUId(id);
    }

    @Override
    public List getUserList() {
        return userDao.getAll();
    }

    @Override
    public Object addUser(IntermediaryUsers user) {
        return userDao.insert(user);
    }

    @Override
    public void updateUser(IntermediaryUsers user) {
        userDao.update(user);
    }

    @Override
    public void deleteUser(Integer id) {
        userDao.deleteByUId(id);
    }   

    @Transactional(readOnly = true)
    @Override
    public List getUserListBykeyword(String keyword) {
        return userDao.getPartUserByKeyword(keyword);
    }
}
