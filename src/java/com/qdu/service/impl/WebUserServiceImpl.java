package com.qdu.service.impl;

import com.qdu.pojo.WebUsers;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.qdu.dao.WebUserDao;
import com.qdu.pojo.WebUsers;
import com.qdu.service.WebUserService;

@Service
@Transactional
public class WebUserServiceImpl implements WebUserService{
    
    @Autowired
    private WebUserDao webuserDao;

//    @Transactional(readOnly = true)

    @Override
    public WebUsers getWebUserByWId(Integer id) {
        return webuserDao.getOneByWId(id);
    }

    @Override
    public List getWebUserList() {
        return webuserDao.getAll();
    }

    @Override
    public Object addWebUser(WebUsers webuser) {
        return webuserDao.insert(webuser);
    }

    @Override
    public void updateWebUser(WebUsers webuser) {
        webuserDao.update(webuser);
    }

    @Override
    public void deleteWebUser(Integer id) {
        webuserDao.deleteByWId(id);
    }
    
}
