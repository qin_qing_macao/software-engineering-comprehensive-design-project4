package com.qdu.service.impl;

import com.qdu.dao.HouseDao;
import com.qdu.pojo.HouseImfo;
import com.qdu.service.HouseService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional
public class HouseServiceImpl implements HouseService{
    @Autowired
    private HouseDao houseDao;
    
    
    @Override
    public List getHouseList() {
        return houseDao.getAll();
    }

    @Override
    public void updateHouse(HouseImfo house) {
        houseDao.update(house);
    }

    @Override
    public void deleteHouse(String id) {
        houseDao.deleteByHId(id);
    }

    @Override
    public List getUserListBykeyword(String keyword) {
        return houseDao.getPartHouseByKeyword(keyword);
    }

    @Override
    public HouseImfo getHouseByUId(String id) {
        return houseDao.getOneByUId(id);
    }
    
}
