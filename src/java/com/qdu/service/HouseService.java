package com.qdu.service;

import com.qdu.pojo.HouseImfo;
import com.qdu.pojo.IntermediaryUsers;
import java.util.List;

public interface HouseService {
    
    //根据房屋编号获得一个房屋的全部信息
    HouseImfo getHouseByUId(String id);
    
    //获取房源信息列表
    List getHouseList();

    //更新房源信息
    void updateHouse(HouseImfo house);

    //根据房屋编号删除房屋信息
    void deleteHouse(String id);
    
    //根据关键字检索房源信息
    List getUserListBykeyword(String keyword);
}
