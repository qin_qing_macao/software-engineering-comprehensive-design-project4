/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qdu.service;

import com.qdu.pojo.WebUsers;
import java.util.List;

/**
 *
 * @author dell
 */
public interface WebUserService {
    //根据用户编号获得一个用户的全部信息
    WebUsers getWebUserByWId(Integer id);
    
    //获取所有用户列表
    List getWebUserList();

    //添加一个新用户
    Object addWebUser(WebUsers webuser);

    //更新用户信息
    void updateWebUser(WebUsers webuser);

    //根据用户姓名删除一个用户
    void deleteWebUser(Integer id);
}
