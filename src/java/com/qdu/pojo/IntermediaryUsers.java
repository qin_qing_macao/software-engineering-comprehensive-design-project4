
package com.qdu.pojo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class IntermediaryUsers implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer uId;
    private String uName;
    private String uPhone;
    private String uEmail;
    private String uBirthday;
    private String uSex;
    private String uPwd;
    private String uImage;
    private String keyword;

    public IntermediaryUsers() {
    }

    public IntermediaryUsers(Integer uId) {
        this.uId = uId;
    }

    public IntermediaryUsers(Integer uId, String uName, String uPhone, String uEmail, String uBirthday, String uSex, String uPwd, String uImage, String keyword) {
        this.uId = uId;
        this.uName = uName;
        this.uPhone = uPhone;
        this.uEmail = uEmail;
        this.uBirthday = uBirthday;
        this.uSex = uSex;
        this.uPwd = uPwd;
        this.uImage = uImage;
        this.keyword = keyword;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public String getuEmail() {
        return uEmail;
    }

    public void setuEmail(String uEmail) {
        this.uEmail = uEmail;
    }

    public String getuBirthday() {
        return uBirthday;
    }

    public void setuBirthday(String uBirthday) {
        this.uBirthday = uBirthday;
    }

    public String getuSex() {
        return uSex;
    }

    public void setuSex(String uSex) {
        this.uSex = uSex;
    }

    public String getuPwd() {
        return uPwd;
    }

    public void setuPwd(String uPwd) {
        this.uPwd = uPwd;
    }

    public String getuImage() {
        return uImage;
    }

    public void setuImage(String uImage) {
        this.uImage = uImage;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    

}
