/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.qdu.pojo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author dell
 */
@Entity
public class WebUsers implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer wId;
    private String wName;
    private String wPhone;
    private String wEmail;
    private String wBirthday;
    private String wSex;
    private String wPwd;
    private String wImage;

    public WebUsers() {
    }
    
    public WebUsers(Integer wId) {
        this.wId = wId;
    }

    public WebUsers(Integer wId, String wName, String wPhone, String wEmail, String wBirthday, String wSex, String wPwd, String wImage) {
        this.wId = wId;
        this.wName = wName;
        this.wPhone = wPhone;
        this.wEmail = wEmail;
        this.wBirthday = wBirthday;
        this.wSex = wSex;
        this.wPwd = wPwd;
        this.wImage = wImage;
    }

    public Integer getwId() {
        return wId;
    }

    public void setwId(Integer wId) {
        this.wId = wId;
    }

    public String getwName() {
        return wName;
    }

    public void setwName(String wName) {
        this.wName = wName;
    }

    public String getwPhone() {
        return wPhone;
    }

    public void setwPhone(String wPhone) {
        this.wPhone = wPhone;
    }

    public String getwEmail() {
        return wEmail;
    }

    public void setwEmail(String wEmail) {
        this.wEmail = wEmail;
    }

    public String getwBirthday() {
        return wBirthday;
    }

    public void setwBirthday(String wBirthday) {
        this.wBirthday = wBirthday;
    }

    public String getwSex() {
        return wSex;
    }

    public void setwSex(String wSex) {
        this.wSex = wSex;
    }

    public String getwPwd() {
        return wPwd;
    }

    public void setwPwd(String wPwd) {
        this.wPwd = wPwd;
    }

    public String getwImage() {
        return wImage;
    }

    public void setwImage(String wImage) {
        this.wImage = wImage;
    }
    

}
