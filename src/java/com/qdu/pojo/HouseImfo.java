package com.qdu.pojo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class HouseImfo implements Serializable{
    @Id
    private String hid;
    private Integer uId;
    private Float price;
    private String location;
    private String houseType;
    private Float area;
    private String address;
    private String keyword;
    private String hpic;

    public HouseImfo() {
    }

    public HouseImfo(String hid) {
        this.hid = hid;
    }
    public HouseImfo(String hid, Integer uId, Float price, String location, String houseType, Float area, String address, String keyword, String hpic) {
        this.hid = hid;
        this.uId = uId;
        this.price = price;
        this.location = location;
        this.houseType = houseType;
        this.area = area;
        this.address = address;
        this.keyword = keyword;
        this.hpic = hpic;
    }

    public String getHid() {
        return hid;
    }

    public void setHid(String hid) {
        this.hid = hid;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    public Float getArea() {
        return area;
    }

    public void setArea(Float area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getHpic() {
        return hpic;
    }

    public void setHpic(String hpic) {
        this.hpic = hpic;
    }
    
}
